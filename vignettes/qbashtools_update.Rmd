---
title: "Update qbashtools"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Update qbashtools}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

# Disclaimer
The steps required to update the `qbashtools` scripts are documented. The update is done on `20220131`.


# Update 20220131

* Step 1: Rename old version

```
cd /qualstorzws01/data_projekte/linuxBin
mv qbashtools/ qbashtools.org
```

* Step 2: Run Update Script 
From mac

```
cd;cd Data/Projects/Github/fbzwsqualitasag/qbashtools
./inst/bash/update_qbashtools.sh -s wolga
```

* Step 3: Update Links
From linux

```
cd /qualstorzws01/data_projekte/linuxBin
rm ccpr
ln -s qbashtools/inst/bash/create_comparison_plot_report.sh ccpr
rm create_readme
ln -s qbashtools/inst/bash/create_readme.sh create_readme
rm monitor_job.sh
ln -s qbashtools/inst/bash/monitor_job.sh monitor_job
ln -s qbashtools/inst/bash/nbsaro.sh nbsaro
ln -s qbashtools/inst/bash/nbsasc.sh nbsasc
ln -s qbashtools/inst/bash/nbutsc.sh nbutsc
rm nbsaro.sh nbsasc.sh nbutsc.sh
```

* Step 4: Run Tests
Create new project `qb_update_test` and put a README in the project

```
cd /qualstorzws01/data_tmp
create_readme -p qb_update_test -r
```

Creation of a comparison plot report

```
mkdir ccpr
cd ccpr
cp -r /qualstorzws01/data_zws/fbk/work/bv/YearMinus0/compareBull compareBull_2112
cp -r /qualstorzws01/data_archiv/zws/2108/fbk/work/bv/YearMinus0/compareBull/ compareBull_2108/
# unzip archived plots
ls -1 compareBull_2108/*.gz | while read f;do echo " * Unzipping $f ...";gunzip $f;sleep 2;done
# delete old reports
rm compareBull_2108/ge_plot_report_fbk_compareBull_bv.pdf compareBull_2112/ge_plot_report_fbk_compareBull_bv.pdf 
# run report
ccpr -l compareBull_2108 -r compareBull_2112
```

New bash scripts

```
nbsaro -o new_sa_routine.sh
nbsasc -o new_sa_script.sh
nbutsc -o new_ut_script.sh
```

Monitoring jobs

```
monitor_job -d /qualstororatest01/argus_aebi/qualitas/zws -l 20220131093349_export_pedi_errexit_wolga.log -t
```


