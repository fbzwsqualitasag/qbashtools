#' Build Package Site
#' 
#' @description 
#' The site for the package is built including all the reference 
#' to the html-documents of the bash scripts.
#'
#' @export build_site
build_site <- function(){
  qbashtools::template_navbar_bash_ref(ps_pkgdown_path = '.',ps_nb_yml_outfile = 'pkgdown/_navbar.yml', pb_force_output = TRUE)
  qbashtools::build_bash_ref(ps_script_path = file.path(here::here(), 'inst', 'bash'), 
                             ps_out_dir     = file.path(here::here(), 'docs', 'bash_ref'),
                             ps_navbar_path = file.path(here::here(), 'pkgdown', '_navbar.yml'), 
                             pb_force_output = TRUE)
  pkgdown::build_site()
  return(invisible(NULL))
}
