#!/bin/bash
#' ---
#' title: Configure Parameter File
#' date:  2021-11-11 10:16:36
#' author: Peter von Rohr
#' ---
#' ## Purpose
#' Seamless change of values in parameterfiles
#'
#' ## Description
#' Configure an existing parameter file
#'
#' ## Details
#' The configuration is done by reading the existing parameter file and ask the user whether any parameter values should be changed
#'
#' ## Example
#' ./configure_parfile.sh -p <par_file>
#'
#' ## Set Directives
#' General behavior of the script is driven by the following settings
#+ bash-env-setting, eval=FALSE
set -o errexit    # exit immediately, if single command exits with non-zero status
set -o nounset    # treat unset variables as errors
set -o pipefail   # return value of pipeline is value of last command to exit with non-zero status
                  #  hence pipe fails if one command in pipe fails


#' ## Global Constants
#' ### Paths to shell tools
#+ shell-tools, eval=FALSE
ECHO=/bin/echo                             # PATH to echo                            #
DATE=/bin/date                             # PATH to date                            #
MKDIR=/bin/mkdir                           # PATH to mkdir                           #
BASENAME=/usr/bin/basename                 # PATH to basename function               #
DIRNAME=/usr/bin/dirname                   # PATH to dirname function                #

#' ### Directories
#' Installation directory of this script
#+ script-directories, eval=FALSE
INSTALLDIR=`$DIRNAME ${BASH_SOURCE[0]}`    # installation dir of bashtools on host   #

#' ### Files
#' This section stores the name of this script and the
#' hostname in a variable. Both variables are important for logfiles to be able to
#' trace back which output was produced by which script and on which server.
#+ script-files, eval=FALSE
SCRIPT=`$BASENAME ${BASH_SOURCE[0]}`       # Set Script Name variable                #
SERVER=`hostname`                          # put hostname of server in variable      #



#' ## Functions
#' The following definitions of general purpose functions are local to this script.
#'
#' ### Usage Message
#' Usage message giving help on how to use the script.
#+ usg-msg-fun, eval=FALSE
usage () {
  local l_MSG=$1
  $ECHO "Usage Error: $l_MSG"
  $ECHO "Usage: $SCRIPT -p <par_file> -v"
  $ECHO "  where -p <par_file>  --  parameter file that needs to be configured ..."
  $ECHO "        -v                 (optional)  verbose output ..."
  $ECHO ""
  exit 1
}

#' ### Start Message
#' The following function produces a start message showing the time
#' when the script started and on which server it was started.
#+ start-msg-fun, eval=FALSE
start_msg () {
  $ECHO "********************************************************************************"
  $ECHO "Starting $SCRIPT at: "`$DATE +"%Y-%m-%d %H:%M:%S"`
  $ECHO "Server:  $SERVER"
  $ECHO
}

#' ### End Message
#' This function produces a message denoting the end of the script including
#' the time when the script ended. This is important to check whether a script
#' did run successfully to its end.
#+ end-msg-fun, eval=FALSE
end_msg () {
  $ECHO
  $ECHO "End of $SCRIPT at: "`$DATE +"%Y-%m-%d %H:%M:%S"`
  $ECHO "********************************************************************************"
}

#' ### Log Message
#' Log messages formatted similarly to log4r are produced.
#+ log-msg-fun, eval=FALSE
log_msg () {
  local l_CALLER=$1
  local l_MSG=$2
  local l_RIGHTNOW=`$DATE +"%Y%m%d%H%M%S"`
  $ECHO "[${l_RIGHTNOW} -- ${l_CALLER}] $l_MSG"
}


#' ## Main Body of Script
#' The main body of the script starts here with a start script message.
#+ start-msg, eval=FALSE
start_msg

#' ## Getopts for Commandline Argument Parsing
#' If an option should be followed by an argument, it should be followed by a ":".
#' Notice there is no ":" after "h". The leading ":" suppresses error messages from
#' getopts. This is required to get my unrecognized option code to work.
#+ getopts-parsing, eval=FALSE
PAR_FILE=''
VERBOSE='FALSE'
while getopts ":p:vh" FLAG; do
  case $FLAG in
    h)
      usage "Help message for $SCRIPT"
      ;;
    p)
      if test -f $OPTARG; then
        PAR_FILE=$OPTARG
      else
        usage "$OPTARG isn't a regular file"
      fi
      ;;
    v)
      VERBOSE='TRUE'
      ;;
    :)
      usage "-$OPTARG requires an argument"
      ;;
    ?)
      usage "Invalid command line argument (-$OPTARG) found"
      ;;
  esac
done

shift $((OPTIND-1))  #This tells getopts to move on to the next argument.


#' ## Configure Parameter File
#' Scan the parameter file and ask user whether to change value
#+ configure-parameter-file
if [ "$VERBOSE" == 'TRUE' ];then log_msg $SCRIPT " * Rename $PAR_FILE to $PAR_FILE.org ...";fi
mv $PAR_FILE $PAR_FILE.org
lines=()
while read line
do
  if [ "$VERBOSE" == 'TRUE' ];then log_msg $SCRIPT " * Adding line: $line ...";fi
  lines+=( ${line} )
done < <(cat $PAR_FILE.org)


#' ## Loop Over Lines
#' Lopp over lines collected in old configuration file
#+ loop-conf-lines
for i in ${!lines[@]}
do
  CURLINE=${lines[i]}
  if [ "$VERBOSE" == 'TRUE' ];then log_msg $SCRIPT " * Reading line: $CURLINE ...";fi
  PARAM_NAME=$(echo $CURLINE | cut -d '=' -f1)
  PARAM_VALUE=$(echo $CURLINE | cut -d '=' -f2)
  if [ "$VERBOSE" == 'TRUE' ];then log_msg $SCRIPT " * Parameter name: $PARAM_NAME ...";fi
  if [ "$VERBOSE" == 'TRUE' ];then log_msg $SCRIPT " * Parameter value: $PARAM_VALUE ...";fi
  # ask user
  read -p " * $PARAM_NAME [$PARAM_VALUE]: " inputvalue
  if [ -z "$inputvalue" ]
  then
    replacevalue=$PARAM_VALUE
  else
    replacevalue=$inputvalue
  fi
  
  echo "${PARAM_NAME}=${replacevalue}" >> $PAR_FILE
  if [ "$VERBOSE" == 'TRUE' ];then log_msg $SCRIPT " * Writing ${PARAM_NAME}=${replacevalue} to $PAR_FILE ...";fi
done


#' ## Clean Up
#' Clenaing up original of parameter file
#+ clean-up
if [ "$VERBOSE" == 'TRUE' ];then log_msg $SCRIPT " * Clean up $PAR_FILE.org ...";fi
rm -rf $PAR_FILE.org


#' ## End of Script
#' This is the end of the script with an end-of-script message.
#+ end-msg, eval=FALSE
end_msg

