# qbashtools 0.1.0

* Deployment test for gitlab pages

# qbashtools 0.0.9

* Updated Links for submitting issues

# qbashtools 0.0.8

* Added scripts to create and to configure parameter files

# qbashtools 0.0.7

* Fixed bug in create_readme not working without commandline arguments

# qbashtools 0.0.6

* Added bash-script to return the complete list of servers
* Added bash-script to report top dumps of remote servers
* Added R-function to directly build package web-site

# qbashtools 0.0.5

* Added vignette on creating readme files for projects
* Added option -r to create_readme.sh that creates non-existing project directories

# qbashtools 0.0.4

* Added functionality to create template navbar

# qbashtools 0.0.3

* Documentation feature added and integrated into website generated by pkgdown

# qbashtools 0.0.2

* Released first version with wrapper scripts and with article on how to get started

# qbashtools 0.0.1

* Added a `NEWS.md` file to track changes to the package.
* Started to release first version under new github accout fbzwsqualitasag
