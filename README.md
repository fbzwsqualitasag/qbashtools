
<!-- README.md is generated from README.Rmd. Please edit that file -->

# qbashtools

<!-- badges: start -->
<!-- badges: end -->

## Package website

The package website is available under:
<https://fbzwsqualitasag.gitlab.io/qbashtools/>

## Goal

The goal of qbashtools is to have a collection of bash tools available
that can be used to solve standard tasks such as

-   creating new bash scripts from pre-defined template files
-   creating documentations based on script source files
-   creating new project directories, including README files which give
    a short description of a project
-   providing functionalities related to monitoring running processes
